#version 330

uniform mat4 worldViewProj;

in vec2 uv0;
in vec3 normal;
in vec3 vertex;

out vec2 iUv;
out vec3 iNormal;

void main(void)
{
	gl_Position = worldViewProj*vec4(vertex,1.0);
	
	iUv = uv0;
	iNormal = (normal+1f)*0.5f;
}