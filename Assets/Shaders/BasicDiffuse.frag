#version 330

uniform sampler2D mainTex;

in vec2 iUv;
in vec3 iNormal;

out vec3 oColor;

void main (void)  
{
	//oColor = mix(vec3(iUv.xy,0.0),iNormal,0.5);
	oColor = texture(mainTex,iUv).rgb;
}