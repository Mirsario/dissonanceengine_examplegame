﻿using Dissonance.Engine;
using Dissonance.Engine.Audio;
using Dissonance.Engine.Graphics;
using ExampleGame.Components;

namespace ExampleGame.Objects
{
	public class CameraObj : GameObject
	{
		public override void OnInit()
		{
			AddComponent<Camera>();
			AddComponent<AudioListener>();
			AddComponent<CameraMovementController>();
		}
	}
}
