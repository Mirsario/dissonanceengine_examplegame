using System.Collections.Generic;
using Dissonance.Engine;
using Dissonance.Engine.Audio;
using Dissonance.Engine.Graphics;
using Dissonance.Engine.Input;
using Dissonance.Engine.IO;
using Dissonance.Framework.Graphics;
using Dissonance.Framework.Windowing.Input;
using ExampleGame.Objects;

namespace ExampleGame
{
	public class Main : Game
	{
		public static readonly Dictionary<string, string> DebugStrings = new Dictionary<string, string>();

		public override void PreInit()
		{
			Name = "ExampleGame";
			DisplayName = "Example Game";

			Texture.defaultFilterMode = FilterMode.Bilinear;
			Texture.defaultWrapMode = TextureWrapMode.ClampToBorder;

			Rendering.SetRenderingPipeline<DeferredRendering>();
		}
		public override void Start()
		{
			GameInput.Initialize();

			GameObject.Instantiate<CameraObj>(obj => {
				obj.Name = "Camera";
				obj.Transform.Position = new Vector3(0f, 0f, -10f);
			});

			for(int i = 0; i < 10; i++) {
				const float Offset = 4f;

				var cube = GameObject.Instantiate<TestCube>(obj => obj.Transform.Position = new Vector3(
					Rand.Range(-Offset, Offset),
					Rand.Range(-Offset, Offset),
					Rand.Range(-Offset, Offset)
				));

				if(i == 0) {
					var music = cube.AddComponent<AudioSource>(c => {
						c.Clip = Resources.Get<AudioClip>("Ikachan.ogg");
						c.Loop = true;
					});

					music.Play();
				}
			}

			Rendering.ambientColor = Vector3.Zero;
		}
		public override void FixedUpdate()
		{
			Rendering.DebugFramebuffers = InputEngine.GetKey(Keys.F);

			if(InputEngine.GetKeyDown(Keys.Escape)) {
				if(Screen.lockCursor) {
					Screen.lockCursor = false;
				} else {
					Quit();
				}
			}
		}
		public override void OnGUI()
		{
			float y = 0.5f;

			RectFloat Rect() => new RectFloat(8, y++ * 16, 128, 8);

			DebugStrings["Render FPS"] = Time.RenderFramerate.ToString();
			DebugStrings["Render MS"] = Time.RenderMs.ToString("0.00");
			DebugStrings["Logic FPS"] = Time.FixedFramerate.ToString();
			DebugStrings["Logic MS"] = Time.FixedMs.ToString("0.00");
			DebugStrings["Draw Calls Count"] = Rendering.drawCallsCount.ToString();
			DebugStrings["Controls"] = "Use WASD, Space & C to move around.";

			foreach(var pair in DebugStrings) {
				GUI.DrawText(Rect(), $"{pair.Key}: {pair.Value}");
			}

			DebugStrings.Clear();
		}
	}
}