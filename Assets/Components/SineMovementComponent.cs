﻿using Dissonance.Engine;

namespace ExampleGame.Components
{
	public class SineMovementComponent : Component
	{
		private Vector3 startPosition;
		private float randomTimeOffset;

		protected override void OnInit()
		{
			startPosition = Transform.Position;
			randomTimeOffset = Rand.Next(1000f);
		}
		protected override void RenderUpdate()
		{
			float time = Time.RenderGameTime + randomTimeOffset;

			Transform.Position = startPosition + new Vector3(Mathf.Sin(time * 2f), Mathf.Sin(time), Mathf.Cos(time * 0.5f));
			Transform.EulerRot = new Vector3(Mathf.Sin(time * 2f), Mathf.Sin(time), Mathf.Cos(time * 0.5f)) * 20f;
		}
	}
}
