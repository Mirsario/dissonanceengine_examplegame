﻿using Dissonance.Engine.Input;
using Dissonance.Framework.Windowing.Input;

namespace ExampleGame
{
	public static class GameInput
	{
		//Movement
		public static InputTrigger MoveX { get; private set; }
		public static InputTrigger MoveY { get; private set; }
		public static InputTrigger Jump { get; private set; }
		public static InputTrigger Crouch { get; private set; }

		public static void Initialize()
		{
			MoveX = InputEngine.RegisterTrigger("MoveX", new InputBinding[] { "-A", "+D", "GamePad0 Axis0" }, -1f, 1f);
			MoveY = InputEngine.RegisterTrigger("MoveY", new InputBinding[] { "+S", "-W", "-GamePad0 Axis1" }, -1f, 1f);
			Jump = InputEngine.RegisterTrigger("Jump", new InputBinding[] { Keys.Space, "GamePad0 Button0" });
			Crouch = InputEngine.RegisterTrigger("Crouch", new InputBinding[] { Keys.C, "GamePad0 Button9" });
		}
	}
}
