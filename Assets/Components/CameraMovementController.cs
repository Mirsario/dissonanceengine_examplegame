﻿using Dissonance.Engine;

namespace ExampleGame.Components
{
	public class CameraMovementController : Component
	{
		protected override void RenderUpdate()
		{
			var position = Transform.Position;

			position += new Vector3(
				GameInput.MoveX.Value,
				GameInput.Jump.Value - GameInput.Crouch.Value,
				-GameInput.MoveY.Value
			) * 5f * Time.RenderDeltaTime;

			Transform.Position = position;

			Main.DebugStrings["Camera Pos"] = position.ToString();
		}
	}
}
