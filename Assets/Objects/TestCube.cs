﻿using Dissonance.Engine;
using Dissonance.Engine.Graphics;
using Dissonance.Engine.IO;
using ExampleGame.Components;

namespace ExampleGame.Objects
{
	public class TestCube : GameObject
	{
		public override void OnInit()
		{
			AddComponent<MeshRenderer>(c => {
				c.Mesh = PrimitiveMeshes.Sphere;
				c.Material = Resources.Find<Material>("TestCube");
			});
			AddComponent<Light>(c => c.color = new Vector3(Rand.Next(1f), Rand.Next(1f), Rand.Next(1f)).Normalized);
			AddComponent<SineMovementComponent>();
		}
	}
}
