﻿#pragma warning disable 219

namespace ExampleGame
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			using var main = new Main();

			main.Run(args:args);
		}
	}
}